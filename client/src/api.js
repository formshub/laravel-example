import axios from 'axios'
import ResponseCode from './dict/responseCode'

axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? '/api' : `${process.env.VUE_APP_URL || ''}/api`

axios.interceptors.request.use((config) => {
    // задрало пробрасывать по всем компонентам
    if (window.vm.$store.state.axios.checkDisabled) {
        // eslint-disable-next-line no-param-reassign
        config.headers['check-disabled'] = true
    }

    if (config.tag) {
        window.vm.$store.commit('setTagLoading', config.tag)
    }

    return config
}, (error) => {
    const newError = error
    return Promise.reject(newError)
})

axios.interceptors.response.use((response) => {
    const newResponse = response
    if (response.request.responseType !== 'blob') {
        newResponse.paginate = response.data.paginate
        newResponse.data = response.data.result
    }
    if (response.config.tag) {
        window.vm.$store.commit('unsetTagLoading', response.config.tag)
    }
    return newResponse
}, (error) => {
    // console.log('error', error)
    const newError = error
    const config = error.response.config || {}

    if (error.response && error.response.data) {
        if (error.response.status === ResponseCode.HTTP_INTERNAL_SERVER_ERROR) {
            window.vm.$message.error({
                // message: `Внутренняя ошибка сервера (${error.response.status})`,
                message: 'Внутренняя ошибка сервера',
                showClose: true,
                duration: 10 * 1000
            })
        } else if (error.response.status === ResponseCode.HTTP_FORBIDDEN) {
            if (window.vm.$store.state.axios.check403) {
                window.vm.$router.replace({ name: 'forbidden' })
            } else {
                window.vm.$message.error({
                    message: 'Недостаточно привилегий для просмотра страницы!',
                    showClose: true,
                    duration: 10 * 1000
                })
            }
        } else if (config.check404 && error.response.status === ResponseCode.HTTP_NOT_FOUND) {
            window.vm.$router.replace({ name: 'not_found' })
        } else if (error.response.data.result) {
            newError.response.data = error.response.data.result
        } else {
            window.vm.$message.warning({
                // message: `${error.response.data.message} (${error.response.status})`,
                message: error.response.data.message,
                showClose: true,
                duration: 5 * 1000
            })
        }
    }
    if (config.tag) {
        window.vm.$store.commit('unsetTagLoading', error.response.config.tag)
    }
    return Promise.reject(newError)
})

export default axios

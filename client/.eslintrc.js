const path = require('path')

module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: ['plugin:vue/essential', '@vue/airbnb'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        indent: ['error', 4],
        semi: ['error', 'never'],
        'comma-dangle': ['error', 'never'],
        'linebreak-style': 0,
        'consistent-return': 'off',
        'prefer-destructuring': ['error', {
            AssignmentExpression: {
                array: false
            }
        }],
        'import/order': 0,
        'import/no-cycle': 0,
        'vue/valid-v-model': 0,
        'no-param-reassign': ["error", { "props": false }]
    },
    parserOptions: {
        parser: 'babel-eslint'
    },
    overrides: [
        {
            files: ['*.vue', '*.js'],
            rules: {
                indent: 0,
                'max-len': ['error', {
                    code: 120,
                    ignoreStrings: true,
                    ignorePattern: 'd="([\\s\\S]*?)"'
                }]
                // 'vue/script-indent': ['error', 4, { 'baseIndent': 4 }]
            }
        }
    ],
    globals: {
        _: 'readonly'
    }
}

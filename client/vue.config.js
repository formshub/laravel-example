const path = require('path')
const CopyPlugin = require('copy-webpack-plugin')
const CreateFileWebpack = require('create-file-webpack')

const {
    PORT
} = process.env
const isDev = process.env.NODE_ENV !== 'production'
const isProd = process.env.NODE_ENV === 'production'

const publicPath = isProd ? '/' : `http://localhost${(PORT ? `:${PORT}` : '')}`
const outputDir = '../public'

const buildSettings = {
    NODE_ENV: process.env.NODE_ENV,
    publicPath
}

const createFileOptions = {
    path: outputDir,
    fileName: 'build_settings.json',
    content: JSON.stringify(buildSettings)
}

module.exports = {
    publicPath,
    outputDir,
    productionSourceMap: false,

    css: {
        sourceMap: isDev
    },

    configureWebpack: {
        plugins: [
            // в режиме development (npm run serve) копирует не в public на диске
            // а в дев-сервер http://localhost:8080/public
            new CopyPlugin([
                {
                    from: '../resources/public',
                    to: outputDir
                }
            ]),
            new CreateFileWebpack(createFileOptions)
        ],
        devServer: {
            disableHostCheck: true,
            port: PORT,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            overlay: {
                warnings: false,
                errors: false
            }
        }
    },

    chainWebpack: (config) => {
        config.resolve.symlinks(false)

        config.resolve.alias
            .set('@', path.resolve(__dirname, 'src'))

        const rules = ['vue-modules', 'vue', 'normal-modules', 'normal']

        rules.forEach((rule) => {
            config.module.rule('scss')
                .oneOf(rule)
                .use('resolve-url-loader')
                .loader('resolve-url-loader')
                .before('sass-loader')
                .end()
                .use('sass-loader')
                .loader('sass-loader')
                .tap(options => ({
                    ...options,
                    sassOptions: {
                        quietDeps: true
                    },
                    sourceMap: true
                }))
        })
    }
}
